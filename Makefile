.PHONY: all clean fclean re

CC = clang++
NAME = avm
OBJ =  main.o VirtualMachine.o OperandHandler.o exceptions.o
CPPFLAGS = -Wall -Wextra -Werror
INC = OperandHandler.h IOperand.h eOperandType.h main.h VirtualMachine.h Operand.h

all: $(NAME)

$(NAME): $(OBJ)
	$(CC) $(OBJ) -o $(NAME)

clean:
	rm -f $(OBJ)

fclean: clean
	rm -f $(NAME)

re: fclean all

$(OBJ): %.o: %.cpp
	clang++ -std=c++11 $(CPPFLAGS) -c $< -o $@


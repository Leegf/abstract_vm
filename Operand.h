/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Operand.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/29 19:37:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/11/11 14:43:39 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ABSTRACT_VM_OPERAND_H
#define ABSTRACT_VM_OPERAND_H

#include "IOperand.h"
#include <limits>
#include <cmath>
#include <cfenv>

template <eOperandType N, typename T = IOperand>
class Operand : public IOperand {
public:
	Operand(const std::string & raw_val, eOperandType type) : _type(type), _rawVal(raw_val) {
		int tmp;
		try {
			 tmp = std::stoi(raw_val);
		}
		catch (std::out_of_range & e) {
			if (raw_val[0] == '-')
				throw underflow("num is out of range");
			else
				throw overflow("num is out of range");
		}
		if (tmp < std::numeric_limits<T>::min())
			throw underflow("num is out of range");
		else if (tmp > std::numeric_limits<T>::max())
			throw overflow("num is out of range");
		_val = tmp;
	}

	Operand(T val, eOperandType type) : _val(val), _type(type), _rawVal(std::to_string(val)){ }

	Operand(const Operand & par) {
		this->_rawVal = par._rawVal;
		this->_val = par._val;
		this->_type = par._type;
	}

	Operand & operator=(Operand par) {
		_swap(*this, par);

		return *this;
	}
	eOperandType getType() const {
		return _type;
	}
	int getPrecision() const {
		return _type;
	}
	std::string const & toString() const{
		return _rawVal;
	}
	Operand const * operator+( IOperand const & rhs) const {
		int tmp = std::stoi(rhs.toString());
		if (_val > 0 && tmp > std::numeric_limits<T>::max() - _val)
			throw overflow("overflow occurred at operator +");
		else if (_val < 0 && tmp < std::numeric_limits<T>::min() - _val)
			throw underflow("underflow occurred at operator +");
		return new Operand(tmp + _val, _type);
	}
	Operand const * operator-( IOperand const & rhs) const {
		int tmp = std::stoi(rhs.toString());
		if (tmp < 0 && _val > (std::numeric_limits<T>::max() + tmp))
			throw overflow("overflow occurred at operator -");
		else if (tmp > 0 && _val < (std::numeric_limits<T>::min() + tmp))
			throw underflow("underflow occurred at operator -");
		return new Operand(_val - tmp, _type);
	}
	Operand const * operator*( IOperand const & rhs) const {
		int tmp = std::stoi(rhs.toString());
		if ((_val == -1) && (tmp == std::numeric_limits<T>::min()))
			throw overflow("overflow occurred at operator *");
		if ((_val == std::numeric_limits<T>::min()) && (tmp == -1))
			throw overflow("overflow occurred at operator *");
		if (tmp == -1 || tmp == 0)
			return new Operand(tmp * _val, _type);
		if (_val > std::numeric_limits<T>::max() / tmp)
			throw overflow("overflow occurred at operator *");
		else if (_val < std::numeric_limits<T>::min() / tmp)
			throw underflow("underflow occurred at operator *");
		return new Operand(tmp * _val, _type);
	}
	Operand const * operator/( IOperand const & rhs) const {
		int tmp = std::stoi(rhs.toString());
		if (tmp == 0)
			throw divByZero("can't divide by zero");
		if ((_val == std::numeric_limits<T>::min()) && (tmp == -1))
			throw overflow("underflow occurred at operator /");
		return new Operand(_val / tmp, _type);
	}
	Operand const * operator%( IOperand const & rhs) const {
		int tmp = std::stoi(rhs.toString());
		if (tmp == 0)
			throw divByZero("can't divide by zero");
		if ((_val == std::numeric_limits<T>::min()) && (tmp == -1))
			throw overflow("underflow occurred at operator %");
		return new Operand(_val % tmp, _type);
	}
private:
	T _val;
	eOperandType _type;
	std::string _rawVal;
	friend void _swap(Operand & first, Operand & sec) {
		std::swap(first._val, sec._val);
		std::swap(first._type, sec._type);
		std::swap(first._rawVal, sec._rawVal);
	}
};

template <>
class Operand<Float> : public IOperand {
public:
	Operand(const std::string & raw_val, eOperandType type) : _type(type), _rawVal(raw_val) {
		float tmp;
		try {
			tmp = std::stof(raw_val);
		}
		catch (std::out_of_range & e) {
			throw overflow("num is out of range");
		}
		_val = tmp;
	}
	Operand(float val, eOperandType type) : _val(val), _type(type), _rawVal(std::to_string(val)){ }

	Operand(const Operand & par) {
		this->_rawVal = par._rawVal;
		this->_val = par._val;
		this->_type = par._type;
	}
	Operand & operator=(Operand par) {
		_swap(*this, par);

		return *this;
	}
	eOperandType getType() const {
		return _type;
	}
	int getPrecision() const {
		return _type;
	}
	std::string const & toString() const{
		return _rawVal;
	}
	Operand const * operator+( IOperand const & rhs) const {
		float tmp = std::stof(rhs.toString());

		feclearexcept(FE_ALL_EXCEPT);
		double res = tmp + _val;
		if (std::fetestexcept(FE_OVERFLOW))
			throw overflow("overflow occurred at operator +");
		if (std::fetestexcept(FE_UNDERFLOW))
			throw overflow("underflow occurred at operator +");
		return new Operand(res, _type);
	}
	Operand const * operator-( IOperand const & rhs) const {
		float tmp = std::stof(rhs.toString());

		feclearexcept(FE_ALL_EXCEPT);
		double res = _val - tmp;
		if (std::fetestexcept(FE_OVERFLOW))
			throw overflow("overflow occurred at operator -");
		if (std::fetestexcept(FE_UNDERFLOW))
			throw overflow("underflow occurred at operator -");
		return new Operand(res, _type);
	}
	Operand const * operator*( IOperand const & rhs) const {
		float tmp = std::stof(rhs.toString());

		feclearexcept(FE_ALL_EXCEPT);
		double res = tmp * _val;
		if (std::fetestexcept(FE_UNDERFLOW))
			throw overflow("underflow occurred at operator *");
		if (std::fetestexcept(FE_OVERFLOW))
			throw overflow("overflow occurred at operator *");
		return new Operand(res, _type);
	}
	Operand const * operator/( IOperand const & rhs) const {
		float tmp = std::stof(rhs.toString());

		feclearexcept(FE_ALL_EXCEPT);
		double res = _val / tmp;
		if (std::fetestexcept(FE_DIVBYZERO))
			throw divByZero("can't divide by ");
		if (std::fetestexcept(FE_OVERFLOW))
			throw overflow("overflow occurred at operator -");
		if (std::fetestexcept(FE_UNDERFLOW))
			throw overflow("underflow occurred at operator -");
		return new Operand(res, _type);
	}
	Operand const * operator%( IOperand const & rhs) const {
		float tmp = std::stof(rhs.toString());

		if (tmp == 0)
			throw divByZero("Can't divide by zero in % operator");
		feclearexcept(FE_ALL_EXCEPT);
		double res = fmod(_val, tmp);
		if (std::fetestexcept(FE_INVALID))
			throw overflow("error occurred at operator %");
		return new Operand(res, _type);
	}
private:
	float _val;
	eOperandType _type;
	std::string _rawVal;
	friend void _swap(Operand & first, Operand & sec) {
		std::swap(first._val, sec._val);
		std::swap(first._type, sec._type);
		std::swap(first._rawVal, sec._rawVal);
	}
};

template <>
class Operand<Double> : public IOperand {
public:
	Operand(const std::string & raw_val, eOperandType type) : _type(type), _rawVal(raw_val) {
		double tmp;
		try {
			tmp = std::stod(raw_val);
		}
		catch (std::out_of_range & e) {
			throw overflow("num is out of range");
		}
		_val = tmp;
	}
	Operand(double val, eOperandType type) : _val(val), _type(type), _rawVal(std::to_string(val)){ }

	Operand(const Operand & par) {
		this->_rawVal = par._rawVal;
		this->_val = par._val;
		this->_type = par._type;
	}
	Operand & operator=(Operand par) {
		_swap(*this, par);

		return *this;
	}
	eOperandType getType() const {
		return _type;
	}
	int getPrecision() const {
		return _type;
	}
	std::string const & toString() const{
		return _rawVal;
	}
	Operand const * operator+( IOperand const & rhs) const {
		double tmp = std::stod(rhs.toString());

		feclearexcept(FE_ALL_EXCEPT);
		double res = tmp + _val;
		if (std::fetestexcept(FE_OVERFLOW))
			throw overflow("overflow occurred at operator +");
		if (std::fetestexcept(FE_UNDERFLOW))
			throw overflow("underflow occurred at operator +");
		return new Operand(res, _type);
	}
	Operand const * operator-( IOperand const & rhs) const {
		double tmp = std::stod(rhs.toString());

		feclearexcept(FE_ALL_EXCEPT);
		double res = _val - tmp;
		if (std::fetestexcept(FE_OVERFLOW))
			throw overflow("overflow occurred at operator -");
		if (std::fetestexcept(FE_UNDERFLOW))
			throw overflow("underflow occurred at operator -");
		return new Operand(res, _type);
	}
	Operand const * operator*( IOperand const & rhs) const {
		double tmp = std::stod(rhs.toString());

		feclearexcept(FE_ALL_EXCEPT);
		double res = tmp * _val;
		if (std::fetestexcept(FE_UNDERFLOW))
			throw overflow("underflow occurred at operator *");
		if (std::fetestexcept(FE_OVERFLOW))
			throw overflow("overflow occurred at operator *");
		return new Operand(res, _type);
	}
	Operand const * operator/( IOperand const & rhs) const {
		double tmp = std::stod(rhs.toString());

		feclearexcept(FE_ALL_EXCEPT);
		double res = _val / tmp;
		if (std::fetestexcept(FE_DIVBYZERO))
			throw divByZero("can't divide by ");
		if (std::fetestexcept(FE_OVERFLOW))
			throw overflow("overflow occurred at operator -");
		if (std::fetestexcept(FE_UNDERFLOW))
			throw overflow("underflow occurred at operator -");
		return new Operand(res, _type);
	}
	Operand const * operator%( IOperand const & rhs) const {
		double tmp = std::stod(rhs.toString());

		if (tmp == 0)
			throw divByZero("Can't divide by zero in % operator");
		feclearexcept(FE_ALL_EXCEPT);
		double res = fmod(_val, tmp);
		if (std::fetestexcept(FE_INVALID))
			throw overflow("error occurred at operator %");
		return new Operand(res, _type);
	}
private:
	double _val;
	eOperandType _type;
	std::string _rawVal;
	friend void _swap(Operand & first, Operand & sec) {
		std::swap(first._val, sec._val);
		std::swap(first._type, sec._type);
		std::swap(first._rawVal, sec._rawVal);
	}
};

#endif //ABSTRACT_VM_OPERAND_H

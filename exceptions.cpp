/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exceptions.cpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/03 13:20:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/11/21 16:01:19 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <exception>
#include <iostream>

class exceptionClass : public std::exception {
public:
	exceptionClass(const char * par) : _par(par) {};
	const char *what() const noexcept {
		return _par.c_str();
	}
	std::string _par;
};

class overflow : public exceptionClass {
public:
	overflow(const char * par) : exceptionClass(par) {};
	const char * what() const noexcept {
		return exceptionClass::what();
	}
};

class underflow : public exceptionClass {
public:
	underflow(const char * par) : exceptionClass(par) {};
	const char * what() const noexcept {
		return exceptionClass::what();
	}
};

class divByZero : public exceptionClass {
public:
	divByZero(const char * par) : exceptionClass(par) {};
	const char * what() const noexcept {
		return exceptionClass::what();
	}
};

class fewPars : public exceptionClass {
public:
	fewPars(const char * par) : exceptionClass(par) {};
	const char * what() const noexcept {
		return exceptionClass::what();
	}
};

class assertErr : public exceptionClass {
public:
	assertErr(const char * par) : exceptionClass(par) {};
	const char * what() const noexcept {
		return exceptionClass::what();
	}
};

class syntaxErr : public exceptionClass {
public:
	syntaxErr(const char * par) : exceptionClass(par) {};
	const char * what() const noexcept {
		return exceptionClass::what();
	}
};

/*
class underflow : public std::exception {
public:
	underflow(const char * par) : _par(par) {};
	const char * what() const noexcept {
		return _par.c_str();
	}
private:
	std::string  _par;
};

class divByZero : public std::exception {
public:
	divByZero(const char * par) : _par(par) {};
	const char * what() const noexcept {
		return _par.c_str();
	}
private:
	std::string  _par;
};

class fewPars : public std::exception {
public:
	fewPars(const char * par) : _par(par) {};
	const char * what() const noexcept {
		return _par.c_str();
	}
private:
	std::string  _par;
};

class assertErr : public std::exception {
public:
	assertErr(const char * par) : _par(par) {};
	const char * what() const noexcept {
		return _par.c_str();
	}
private:
	std::string  _par;
};

class syntaxErr : public std::exception {
public:
	syntaxErr(const char * par) : _par(par) {};
	const char * what() const noexcept {
		return _par.c_str();
	}
private:
	std::string  _par;
};
*/


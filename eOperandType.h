/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   eOperandType.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/29 18:30:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/10/29 18:33:20 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef EOPERANDTYPE_H
# define EOPERANDTYPE_H

enum eOperandType {Int8, Int16, Int32, Float, Double};

#endif

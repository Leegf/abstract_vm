/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   OperandHandler.cpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/29 18:53:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/11/17 14:38:14 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "OperandHandler.h"
#include "Operand.h"

IOperand const* OperandHandler::createOperand(std::string const &value,
		eOperandType type) const {
	return (this->*_arr[type])(value, type);
}

OperandHandler::OperandHandler() {
	_arr[0] = &OperandHandler::createInt8;
	_arr[1] = &OperandHandler::createInt16;
	_arr[2] = &OperandHandler::createInt32;
	_arr[3] = &OperandHandler::createFloat;
	_arr[4] = &OperandHandler::createDouble;
}

IOperand const * OperandHandler::createInt8(std::string const &value,
										   eOperandType type) const {
	return (new Operand<Int8, int8_t>(value, type));
}

IOperand const* OperandHandler::createInt16(std::string const &value,
											eOperandType type) const {
	return (new Operand<Int16, int16_t>(value, type));
}

IOperand const* OperandHandler::createInt32(std::string const &value,
											eOperandType type) const {
	return (new Operand<Int32, int32_t>(value, type));
}

IOperand const* OperandHandler::createFloat(std::string const &value,
											eOperandType type) const {
	return (new Operand<Float>(value, type));
}

IOperand const* OperandHandler::createDouble(std::string const &value,
											 eOperandType type) const {
	return (new Operand<Double>(value, type));
}

OperandHandler::~OperandHandler() { }

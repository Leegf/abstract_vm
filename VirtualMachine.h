/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   VirtualMachine.h                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 19:44:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/11/21 14:27:28 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ABSTRACT_VM_VIRTUALMACHINE_H
#define ABSTRACT_VM_VIRTUALMACHINE_H

#include "main.h"
#include <regex>
#include <sstream>
#include <list>
#include <map>
#include <cmath>
#include "IOperand.h"
#include "OperandHandler.h"

class VirtualMachine {
public:
	VirtualMachine(const VirtualMachine & par) = delete;
	VirtualMachine();
	~VirtualMachine();
	VirtualMachine &operator=(const VirtualMachine & par) = delete;
	void readInput(std::istream & e, bool intRead = false);
//	static VirtualMachine *getInst();

private:
//	static VirtualMachine *inst;
	std::stringstream _errs;
	OperandHandler _opHandler;
	std::deque<std::unique_ptr<IOperand const>> _data;
	std::map<std::string, eOperandType> _pa;

	using ptrToCommands = void (VirtualMachine::*)();
	std::map<std::string, ptrToCommands> _commands;

	void _assert(std::string value, eOperandType type) const;
	void _push(std::string value, eOperandType type);
	void _add();
	void _sub();
	void _div();
	void _mod();
	void _mul();
	void _pop();
	void _dump();
	void _print();
	void _errContainer(size_t numOfLine, const std::string &tmp,
					   const std::string &errMessage);
	void _paCommands(size_t numOfLine, const std::string & tmp, std::smatch & m,
			bool *  err_flag, bool assertFlag);
	bool _checkNNum(const std::string &val) const;
	bool _checkZNum(const std::string &val) const;
	bool _checkNum(const std::string &val, eOperandType type) const;
	bool _comp(std::unique_ptr<IOperand const> const & par1, std::unique_ptr<IOperand const> const & par2) const;
	//bonus part
	void _sqrt();
	void _sqr();
	void _avg();
	void _min();
	void _max();
	void _info();
	void _clear();
};


#endif //ABSTRACT_VM_VIRTUALMACHINE_H

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   VirtualMachine.cpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 19:44:00 by lburlach          #+#    #+#             */
/*   Updated: 2019/01/09 17:08:32 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <functional>
#include "VirtualMachine.h"

VirtualMachine::VirtualMachine() {
	_pa["int8"] = Int8;
	_pa["int16"] = Int16;
	_pa["int32"] = Int32;
	_pa["float"] = Float;
	_pa["double"] = Double;

	_commands["pop"] = &VirtualMachine::_pop;
	_commands["add"] = &VirtualMachine::_add;
	_commands["div"] = &VirtualMachine::_div;
	_commands["mul"] = &VirtualMachine::_mul;
	_commands["mod"] = &VirtualMachine::_mod;
	_commands["sub"] = &VirtualMachine::_sub;
	_commands["dump"] = &VirtualMachine::_dump;
	_commands["print"] = &VirtualMachine::_print;
	//bonuses:
	_commands["sqr"] = &VirtualMachine::_sqr;
	_commands["sqrt"] = &VirtualMachine::_sqrt;
	_commands["avg"] = &VirtualMachine::_avg;
	_commands["min"] = &VirtualMachine::_min;
	_commands["max"] = &VirtualMachine::_max;
	_commands["info"] = &VirtualMachine::_info;
	_commands["clear"] = &VirtualMachine::_clear;
}

VirtualMachine::~VirtualMachine() { }

void VirtualMachine::readInput(std::istream & input, bool intRead) {
	size_t numOfLine = 0;
	std::string tmp;
	std::regex eWhiteSpaces(R"(\s*$)");
	std::regex eWhiteSpaces2(R"(^\s*)");
	std::regex ePCommand(R"(^(push)[ ])");
	std::regex eACommand(R"(^(assert)[ ])");
	std::regex eCommands(R"(pop|add|sub|mul|div|mod|print|dump|sqr|sqrt|avg|min|max|info|clear)");
	std::regex eExit(R"(exit)");
	std::smatch m;
	bool err_flag = false;
	int exit_flag = 0;

	while (std::getline(input, tmp) && ++numOfLine) {
		if (tmp[0] == ';') {
			if (intRead && tmp[1] == ';' && tmp.size() == 2)
				break ;
			continue;
		}
		if (tmp.empty())
			continue ;
		tmp = tmp.substr(0, tmp.find(" ;"));
		
		//quick-fix
		if (std::regex_search(tmp, m, eWhiteSpaces))
			tmp = m.prefix();
		if (std::regex_search(tmp, m, eWhiteSpaces2))
			tmp = m.suffix();
		//quick-fix
		
		if (std::regex_search(tmp, m, ePCommand)) {
			_paCommands(numOfLine, tmp, m, &err_flag, false);
		}
		else if (std::regex_search(tmp, m, eACommand)) {
			_paCommands(numOfLine, tmp, m, &err_flag, true);
		}
		else if (std::regex_match(tmp, m, eCommands)) {
			if (!err_flag)
				(this->*_commands[m.str()])();
		}
		else if (std::regex_match(tmp, eExit) && ++exit_flag)
			break ;
		else {
			err_flag = true;
			_errContainer(numOfLine, tmp, "unknown command");
		}
	}
	if (!exit_flag) {
		err_flag = true;
		_errContainer(numOfLine, tmp, "at the end of the program should be exit command");
	}
	if (err_flag) {
		std::cerr << _errs.str() << '\n';
		throw syntaxErr("There are some syntax errors");
	}
}

void VirtualMachine::_paCommands(size_t numOfLine, const std::string &tmp,
		std::smatch & m, bool * err_flag, bool assertFlag)
{
	std::regex eACommand(R"(^assert[ ])");
	std::regex ePACommandsCon(R"(^int8|^int16|^int32|^float|^double)");
	std::regex eWhiteSpaces(R"((^\s+)|(\s+$))");

	std::string strToParse = m.suffix();
	if (std::regex_search(strToParse, eWhiteSpaces)) {
		*err_flag = true;
		_errContainer(numOfLine, tmp, "int8 and etc commands should be preindented with only one single space");
	}
	else if (std::regex_search(strToParse, m, ePACommandsCon)) {
		std::string numStr = m.suffix();
		if (!_checkNum(numStr, _pa[m.str()]))
		{
			*err_flag = true;
			_errContainer(numOfLine, tmp, "number should be formatted according to the rules");
			return ;
		}
		numStr = numStr.substr(numStr.find('(') + 1, numStr.find_last_of(')') - 1);
		if (assertFlag)
			_assert(numStr, _pa[m.str()]);
		else
			_push(numStr, _pa[m.str()]);
	}
	else {
		*err_flag = true;
		_errContainer(numOfLine, tmp, "after push or assert comes unknown command");
	}
}

bool VirtualMachine::_checkNNum(const std::string &val) const {
	std::regex e(R"(\([-]?[0-9]+\))");

	return (std::regex_match(val, e));
}

bool VirtualMachine::_checkZNum(const std::string &val) const {
	std::regex e(R"(\([-]?[0-9]+\.[0-9]+\))");

	return (std::regex_match(val, e));
}

bool VirtualMachine::_checkNum(const std::string &val, eOperandType type) const {
	if (type < 3)
		return _checkNNum(val);
	else
		return _checkZNum(val);
}

void VirtualMachine::_errContainer(size_t numOfLine, const std::string &tmp,
								   const std::string &errMessage) {
	_errs<<numOfLine<<": "<<tmp<<" -- "<<errMessage<<'\n';
}

void VirtualMachine::_dump(){
	for (const auto &i : _data) {
		std::cout<< i->toString()<<'\n';
	}
}

void VirtualMachine::_pop() {
	if (_data.empty())
		throw fewPars("Pop can't be done if there are no elements in the stack");
	_data.pop_front();
}

void VirtualMachine::_assert(std::string value, eOperandType type) const {
	if (_data.empty())
		throw fewPars("there's no a single element in the stack\n");
	if (type != _data.at(0)->getType())
		throw assertErr("assert error happened");
	if ((type == Int8 || type == Int16 || type == Int32) && (std::stoi(value) != std::stoi(_data.at(0)->toString())))
		throw assertErr("assert error happened");
	else if ((type == Float || type == Double) && (std::stof(value) != std::stof(_data.at(0)->toString())))
		throw assertErr("assert error happened");
}

void VirtualMachine::_push(std::string value, eOperandType type) {
	_data.push_front(std::unique_ptr<IOperand const>
							 (_opHandler.createOperand(value, type)));
}

void VirtualMachine::_print() {
	_assert(_data.at(0)->toString(), Int8);
	std::cout<<static_cast<char>(std::stoi(_data.at(0)->toString()));
}

void VirtualMachine::_add() {
	if (_data.size() < 2)
		throw fewPars("operation add can be done only if there are two or more elements in the stack");
	IOperand const * par;
	if (_data.at(0)->getPrecision() > _data.at(1)->getPrecision())
		par = *_data.at(0) + *_data.at(1);
	else
		par = *_data.at(1) + *_data.at(0);
	_data.pop_front();
	_data.pop_front();
	_data.push_front(std::unique_ptr<IOperand const>(par));
}

void VirtualMachine::_mul() {
	if (_data.size() < 2)
		throw fewPars("operation add can be done only if there are two or more elements in the stack");
	IOperand const * par;
	if (_data.at(0)->getPrecision() > _data.at(1)->getPrecision())
		par = *_data.at(0) * *_data.at(1);
	else
		par = *_data.at(1) * *_data.at(0);
	_data.pop_front();
	_data.pop_front();
	_data.push_front(std::unique_ptr<IOperand const>(par));
}

void VirtualMachine::_sub() {
	if (_data.size() < 2)
		throw fewPars("operation add can be done only if there are two or more elements in the stack");
	IOperand const * par;
//	IOperand const * tmp;
	if (_data.at(1)->getPrecision() > _data.at(0)->getPrecision()) {
		par = *_data.at(1) - *_data.at(0);
	}
	else {
//		auto tmp = std::unique_ptr<IOperand const>(_opHandler.createOperand("-1",
//				_data.at(0)->getType()));
//		tmp = _opHandler.createOperand("-1", _data.at(0)->getType());
		auto tmp = std::unique_ptr<IOperand const>(_opHandler.createOperand(_data.at(1)->toString(), _data.at(0)->getType()));
		par = *tmp - *_data.at(0);
	}
	_data.pop_front();
	_data.pop_front();
	_data.push_front(std::unique_ptr<IOperand const>(par));
}

void VirtualMachine::_div() {
	if (_data.size() < 2)
		throw fewPars("operation add can be done only if there are two or more elements in the stack");
	IOperand const * par;
	if (_data.at(1)->getPrecision() > _data.at(0)->getPrecision())
		par = *_data.at(1) / *_data.at(0);
	else {
		auto tmp = std::unique_ptr<IOperand const>(_opHandler.createOperand(_data.at(1)->toString(), _data.at(0)->getType()));
		par = *tmp / *_data.at(0);
	}
	_data.pop_front();
	_data.pop_front();
	_data.push_front(std::unique_ptr<IOperand const>(par));
}

void VirtualMachine::_mod() {
	if (_data.size() < 2)
		throw fewPars("operation add can be done only if there are two or more elements in the stack");
	IOperand const * par;
	if (_data.at(0)->getPrecision() > _data.at(1)->getPrecision())
		par = *_data.at(0) % *_data.at(1);
	else {
		auto tmp = std::unique_ptr<IOperand const>(_opHandler.createOperand("1", _data.at(1)->getType()));
		par = *(*tmp * *_data.at(0)) % *_data.at(1);
	}
	_data.pop_front();
	_data.pop_front();
	_data.push_front(std::unique_ptr<IOperand const>(par));
}

void VirtualMachine::_sqr() {
	if (_data.empty())
		throw fewPars("operation sqr can be done only if there's at least one parameter");
	IOperand const * par;
	par = *_data.at(0) * *_data.at(0);
	_data.pop_front();
	_data.push_front(std::unique_ptr<IOperand const>(par));
}

void VirtualMachine::_sqrt() {
	if (_data.empty())
		throw fewPars("operation sqrt can be done only if there's at least one parameter");
	double tmp = std::stof(_data.at(0)->toString());
	feclearexcept(FE_ALL_EXCEPT);
	IOperand const * par = _opHandler.createOperand(std::to_string(std::sqrt(tmp)), Double);
	if (std::fetestexcept(FE_INVALID))
		throw overflow("error occurred trying to perform sqrt operation");
	_data.pop_front();
	_data.push_front(std::unique_ptr<IOperand const>(par));
}

void VirtualMachine::_avg() {
	if (_data.empty())
		throw fewPars("operation avg can be done only if there's at least one parameter");
	feclearexcept(FE_ALL_EXCEPT);
	double sum = 0;
	for (auto & i : _data)
		sum += std::stof(i->toString());
	if (std::fetestexcept(FE_INVALID))
		throw overflow("operation avg overflowed");
	sum = sum / _data.size();
	std::cout<<sum<<'\n';
/*	IOperand const * par = _opHandler.createOperand(std::to_string(sum), Double);
	_data.push_front(std::unique_ptr<IOperand const>(par));*/
}

bool VirtualMachine::_comp(std::unique_ptr<IOperand const> const & par1,
		std::unique_ptr<IOperand const> const & par2) const{
	return std::stof(par1->toString()) < std::stof(par2->toString());
};

void VirtualMachine::_min() {
	if (_data.empty())
		throw fewPars("operation avg can be done only if there's at least one parameter");
	IOperand const * par = std::min_element(_data.cbegin(), _data.cend(),
											std::bind(&VirtualMachine::_comp,
													  this,
													  std::placeholders::_1,
													  std::placeholders::_2))->get();

	std::cout<<par->toString()<<'\n';
/*	IOperand const * new_par = _opHandler.createOperand(par->toString(), Double);
	_data.push_front(std::unique_ptr<IOperand const>(new_par));*/
}

void VirtualMachine::_max() {
	if (_data.empty())
		throw fewPars("operation avg can be done only if there's at least one parameter");
	IOperand const * par = std::max_element(_data.cbegin(), _data.cend(),
											std::bind(&VirtualMachine::_comp,
													  this,
													  std::placeholders::_1,
													  std::placeholders::_2))->get();
	std::cout<<par->toString()<<'\n';
/*	IOperand const * new_par = _opHandler.createOperand(par->toString(), Double);
	_data.push_front(std::unique_ptr<IOperand const>(new_par));*/
}

void VirtualMachine::_info() {
	size_t count  = 0;
	for (const auto &i : _data) {
		std::cout<<count++<<": "<<i->toString()<<" | ";
		switch (i->getType()) {
			case Int8:
				std::cout<<"int8";
				break;
			case Int16:
				std::cout<<"int16";
				break;
			case Int32:
				std::cout<<"int32";
				break;
			case Float:
				std::cout<<"float";
				break;
			case Double:
				std::cout<<"double";
				break;
			default:
				break;
		}
		std::cout<<'\n';
	}
}

void VirtualMachine::_clear() {
	_data.clear();
}

/*VirtualMachine* VirtualMachine::getInst() {
	if (!inst)
		inst = new VirtualMachine;
	return inst;
}*/

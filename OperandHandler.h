/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   OperandHandler.h                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/29 18:53:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/11/17 14:26:19 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ABSTRACT_VM_OPERANDHANDLER_H
#define ABSTRACT_VM_OPERANDHANDLER_H

#include "eOperandType.h"
#include "main.h"
#include "Operand.h"

class OperandHandler {
public:
	OperandHandler();
	~OperandHandler();
	OperandHandler(OperandHandler const &src) = default;
	OperandHandler &operator=(OperandHandler const &rhs) = default;
	IOperand const *createOperand(std::string const & value, eOperandType type) const;

	using ptr = IOperand const *(OperandHandler::*)(std::string const & value, eOperandType type) const;
private:
	IOperand const * createInt8( std::string const & value, eOperandType type = Int8) const;
	IOperand const * createInt16( std::string const & value, eOperandType type = Int16) const;
	IOperand const * createInt32( std::string const & value, eOperandType type = Int32) const;
	IOperand const * createFloat( std::string const & value, eOperandType type = Float) const;
	IOperand const * createDouble( std::string const & value, eOperandType type = Double) const;
	ptr _arr[5];
};

#endif //ABSTRACT_VM_OPERANDHANDLER_H

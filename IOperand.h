/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   IOperand.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/29 18:27:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/11/02 23:25:04 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef IOPERAND_H
# define IOPERAND_H

#include "main.h"
#include "eOperandType.h"

class IOperand {
public:
	virtual int getPrecision( void ) const = 0;
	virtual eOperandType getType( void ) const = 0;

	virtual IOperand const * operator+( IOperand const & rhs) const = 0;
	virtual IOperand const * operator-( IOperand const & rhs) const = 0;
	virtual IOperand const * operator*( IOperand const & rhs) const = 0;
	virtual IOperand const * operator/( IOperand const & rhs) const = 0;
	virtual IOperand const * operator%( IOperand const & rhs) const = 0;

	virtual std::string const & toString( void ) const = 0;
	virtual ~IOperand( void ) {}
};

#endif

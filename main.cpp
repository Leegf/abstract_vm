/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/29 18:23:59 by lburlach          #+#    #+#             */
/*   Updated: 2019/01/08 17:36:18 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"
#include <fstream>
#include "VirtualMachine.h"

//VirtualMachine *VirtualMachine::inst = 0;

int		main(int ac, char **av)
{
	if (ac > 2) {
		std::cerr << "There should be one argument or none";
		exit(1);
	}

	try {
		VirtualMachine vm;
		if (ac == 2) {
			std::ifstream fil(av[1]);
			vm.readInput(fil);
			fil.close();
		} else
			vm.readInput(std::cin, true);
	}
	catch (std::exception & e) {
		std::cerr<<"Exception caught: "<<e.what()<<'\n';
	}
	return (0);
}
